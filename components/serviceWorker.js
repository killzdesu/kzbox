export default {
  async alertUpdate (showAlert) {
    const wb = await window.$workbox
    if (wb) {
      wb.addEventListener('waiting', (event) => {
        if (event.isUpdate) {
          wb.addEventListener('controlling', () => {
            window.location.reload()
          })
          showAlert()
        }
      })
    }
  },
  async skipWaiting () {
    const wb = await window.$workbox
    wb.messageSW({ type: 'SKIP_WAITING' })
  },
  async swVersion () {
    const wb = await window.$workbox
    if (wb) {
      const ver = await wb.messageSW({ type: 'GET_VERSION' })
      return ver
    }
    return null
  }
}
