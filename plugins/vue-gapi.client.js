import Vue from 'vue'
import VueGapi from 'vue-gapi'

Vue.use(VueGapi, {
  apiKey: 'AIzaSyATaRGy9PkFPK9NGSRrDWKOqWrBXE_gR8Q',
  clientId: '415215562455-12msfdsq20pjsmmri6cpic024f0annm0.apps.googleusercontent.com',
  // discoveryDocs: ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
  scope: 'https://www.googleapis.com/auth/youtube.readonly',
})
