module.exports = {
  "extends": [
    "@nuxtjs",
    '@nuxtjs/eslint-config-typescript'
  ],
  "rules": {
    'vue/html-indent': 0,
    'comma-dangle': 0
  },
  // parser: "@babel/eslint-parser",
  // parserOptions: {
  //   requireConfigFile: false,
  // },
}
