import colors from 'vuetify/es5/util/colors'

export default {
  // server: {
  //   port: 8000,
  //   host: '0.0.0.0'
  // },
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - kzbox',
    title: 'kzbox',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'msapplication-TileImage', content: '/icons/mstile-150x150.png' },
      { name: 'msapplication-TileColor', content: '#55dddc' },
      { name: 'msapplication-config', content: '/icons/browserconfig.xml' },
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/icons/favicon-16x16.png', sizes: '16x16' },
      { rel: 'icon', type: 'image/png', href: '/icons/favicon-32x32.png', sizes: '32x32' },
      { rel: 'shortcut icon', href: '/icons/favicon.ico' },
      { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css' },
      { rel: 'apple-touch-icon', href: '/icons/apple-touch-icon.png', sizes: "180x180" },
      { rel: 'mask-icon', href: '/icons/safari-pinned-tab.svg', color: "#55dddc" },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // '~/plugins/vue-gapi.client.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    // 'nuxt-purgecss'
    '@nuxt/typescript-build'
  ],

  typescript: {
    // typeCheck: {
    //   eslint: {
    //     files: './**/*.{ts,js,vue}'
    //   }
    // }
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    'nuxt-webfontloader',
    ['@nuxtjs/firebase', {
      config: {
        apiKey: "AIzaSyBMK4JG20t--nP0_XKqq1h-8IzIl_XpeMs",
        authDomain: "kzbox-tool.firebaseapp.com",
        projectId: "kzbox-tool",
        storageBucket: "kzbox-tool.appspot.com",
        messagingSenderId: "966817236312",
        appId: "1:966817236312:web:9d9e4124d872b7f98b112d",
        measurementId: "G-G6N103VY6H"
      },
      services: {
        firestore: true
      }
    }]
  ],

  purgeCSS: {
    path: [`./node_modules/vuetify/dist/vuetify.css`,
      'node_modules/@nuxtjs/vuetify/**/*.ts',
      'node_modules/@nuxt/vue-app/template/**/*.html',
      'node_modules/@nuxt/vue-app/template/**/*.vue'],
    whitelist: ['v-application', 'v-application--wrap'],
    whitelistPatterns: [
      /^v-((?!application).)*$/,
      /^\.theme--light*/,
      /.*-transition/
    ],
    whitelistPatternsChildren: [/^v-((?!application).)*$/, /^theme--light*/]
  },

  webfontloader: {
    google: {
      families: ['Roboto:100,300,400,500,700,900&display=swap']
    }
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    meta: {
      theme_color: '#40E0D0',
      favicon: false,
    },
    manifest: {
      lang: 'en',
      icons: [ 
        {
          "src": "/icons/android-chrome-192x192.png",
          "sizes": "192x192",
          "type": "image/png"
        },
        {
          "src": "/icons/android-chrome-512x512.png",
          "sizes": "512x512",
          "type": "image/png"
        }
      ],
      shortcuts: [ 
        {
          name: "Hololive schedule",
          short_name: "Hololive",
          description: "View Hololive schedule",
          url: "/hololive",
          icons: [
            {
              src: "/icons/hololive-192x192.png",
              sizes: "192x192",
              type: "image/png"
            }
          ]
        }
      ]
    },
    icon:{
      fileName: 'icons/android-chrome-512x512.png',
    },
    workbox: {
      importScripts: [
        'custom-sw.js'
      ],
      config: {
        // debug: true
      },
      skipWaiting: false
    }
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      },
      defaultAssets: false,
      treeShake: true
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: false,
    babel: {
      presets(env, [ preset, options ]) {
        return [
          [ "@nuxt/babel-preset-app", options ]
        ]
      }
    }
  }
}
