export default function ({ route, redirect }) {
  // If browse '/'
  if (route.path === '/') {
    return redirect('/daily')
  }
}
