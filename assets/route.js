module.exports = {
  routes: [
    {
      icon: 'mdi-apps',
      title: 'Welcome',
      to: '/'
    },
    {
      icon: 'mdi-chart-bubble',
      title: 'Inspire',
      to: '/inspire'
    },
    {
      icon: 'mdi-currency-usd',
      title: 'Currency compare',
      to: '/currency'
    },
    {
      icon: 'mdi-checkbox-marked',
      title: 'Daily Reminder',
      to: '/daily'
    },
    {
      icon: 'mdi-dog',
      title: 'Kitsune Command',
      to: '/kitsunecmd'
    },
    {
      icon: 'mdi-google-play',
      title: 'Hololive',
      to: '/hololive'
    }
  ],
}
